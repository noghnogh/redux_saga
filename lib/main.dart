import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_saga/redux_saga.dart';
import 'package:reduxsaga/redux/app_state.dart';
import 'package:reduxsaga/redux/saga.dart';
import 'package:reduxsaga/views/saved_questions_page.dart';
import 'package:reduxsaga/views/question_answer.dart';

import 'redux/app_reducer.dart';

void main() {
  var sagaMiddleware = createSagaMiddleware();

  final store = Store(
    questionAnswerReducer,
    initialState: QAState.initial(),
    middleware: [applyMiddleware(sagaMiddleware)],
  );

  sagaMiddleware.setStore(store);

  sagaMiddleware.run(rootSaga);

  runApp(MyApp(
    store: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store store;

  MyApp({required this.store}) : super();
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        title: 'Flutter Redux-saga demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: QuestionAnswer(),
        routes: <String, WidgetBuilder>{
          '/screen1': (BuildContext context) => new SavedQuestionPage(),
        },
      ),
    );
  }
}
