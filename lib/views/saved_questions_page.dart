import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxsaga/redux/app_state.dart';

class SavedQuestionPage extends StatefulWidget {
  const SavedQuestionPage({Key? key}) : super(key: key);

  @override
  _SavedQuestionPageState createState() => _SavedQuestionPageState();
}

class _SavedQuestionPageState extends State<SavedQuestionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue[700],
        title: Text("Saved Question Page"),
      ),
      body: ListView(
        shrinkWrap: true,
        children: [
          Image.asset(
            "assets/success.jpeg",
            width: 150,
            height: 150,
          ),
          SizedBox(
            height: 100,
          ),
          StoreConnector<dynamic, QAState>(
            converter: (store) => store.state,
            builder: (context, state) {
              return Column(
                children: [
                  ListTile(
                    leading: Image.asset(
                      "assets/question.png",
                      width: 50,
                      height: 50,
                    ),
                    title: Text(
                      state.question,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  ListTile(
                    leading: Image.asset(
                      "assets/answer.png",
                      width: 50,
                      height: 50,
                    ),
                    title: Text(
                      state.answer,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
