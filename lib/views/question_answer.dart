import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxsaga/redux/app_actions.dart';
import 'package:reduxsaga/redux/app_state.dart';
import 'package:reduxsaga/views/question_form.dart';
import 'internet_error.dart';

class QuestionAnswer extends StatefulWidget {
  const QuestionAnswer({Key? key}) : super(key: key);

  @override
  _QuestionAnswerState createState() => _QuestionAnswerState();
}

class _QuestionAnswerState extends State<QuestionAnswer> {
  _moveToQAPage() {
    WidgetsBinding.instance!.addPostFrameCallback(
        (_) => StoreProvider.of(context).dispatch(SuccessSaveAnswer(context)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Hello Redux-saga"),
        backgroundColor: Colors.blue[700],
      ),
      body: StoreConnector<dynamic, QAState>(
        converter: (store) => store.state,
        distinct: true,
        builder: (context, vm) {
          if (vm.isConnected) {
            {
              if (vm.question.length == 0 || vm.answer.length == 0) {
                return QuestionForm();
              } else {
                _moveToQAPage();
                return QuestionForm();
              }
            }
          } else if (!vm.isConnected) {
            return InternetError();
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
