import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxsaga/redux/app_actions.dart';

class QuestionForm extends StatefulWidget {
  const QuestionForm({Key? key}) : super(key: key);

  @override
  _QuestionFormState createState() => _QuestionFormState();
}

class _QuestionFormState extends State<QuestionForm> {
  final TextEditingController _questionController = new TextEditingController();

  final TextEditingController _answerController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView(
        shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: ListTile(
              leading: Image.asset(
                "assets/survey.png",
                width: 50,
                height: 50,
              ),
              title: Text("Please fill your question and answer below"),
            ),
          ),
          TextFormField(
            onChanged: (value) {
              setState(() {});
            },
            controller: _questionController,
            maxLines: 1,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: "Your question",
              fillColor: Colors.white,
              filled: true,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: BorderSide(
                    color: Colors.grey,
                    width: 3.0,
                  )),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  borderSide: BorderSide(
                    color: Color(0xFF999999),
                    width: 1.0,
                  )),
            ),
          ),
          SizedBox(
            height: 50,
          ),
          TextFormField(
            onChanged: (value) {
              setState(() {});
            },
            controller: _answerController,
            maxLines: 1,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: "Your Answer",
              fillColor: Colors.white,
              filled: true,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: BorderSide(
                    color: Colors.grey,
                    width: 3.0,
                  )),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  borderSide: BorderSide(
                    color: Color(0xFF999999),
                    width: 1.0,
                  )),
            ),
          ),
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: () {
                StoreProvider.of(context).dispatch(SaveAsyncAction(
                    _questionController.text, _answerController.text));
              },
              child: new Container(
                height: 50.0,
                decoration: new BoxDecoration(
                    color: Colors.blue[700],
                    borderRadius: new BorderRadius.all(Radius.circular(6.0))),
                child: new Center(
                  child: new Text(
                    "Save Question",
                    style: new TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
