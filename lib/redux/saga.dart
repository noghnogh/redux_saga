import 'package:connectivity/connectivity.dart';
import 'package:redux_saga/redux_saga.dart';
import 'app_actions.dart';

bool isConnected = false;
void checlInternet() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.none) {
    isConnected = false;
  } else {
    isConnected = true;
  }
}

saveAsync({action}) sync* {
  yield Try(() sync* {
    yield Call(checlInternet, args: []);
    yield Put(
        SaveAction(action.actionQuestion, action.actionAnswer, isConnected));
  }, Catch: (e) sync* {
    print("Error while checking internet connection");
  });
}

watchSaveAsync() sync* {
  yield TakeLatest(saveAsync, pattern: SaveAsyncAction);
}

// single entry point to start all Sagas at once
rootSaga() sync* {
  yield All({
    #watch: watchSaveAsync(),
  });
}
