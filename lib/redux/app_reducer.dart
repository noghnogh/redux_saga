import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'app_actions.dart';
import 'app_state.dart';

Reducer<QAState> questionAnswerReducer = combineReducers<QAState>([
  TypedReducer<QAState, SaveAction>(saveAction),
  TypedReducer<QAState, TryAgainAction>(tryAgainAction),
  TypedReducer<QAState, SuccessSaveAnswer>(successSaveAction),
]);

QAState saveAction(QAState state, SaveAction action) {
  if (action.internetConnected) {
    return state.copyWith(
        question: action.actionQuestion,
        answer: action.actionAnswer,
        isConnected: action.internetConnected);
  } else
    return state.copyWith(
        question: state.question,
        answer: state.answer,
        isConnected: action.internetConnected);
}

QAState tryAgainAction(QAState state, TryAgainAction action) {
  return QAState.initial();
}

QAState successSaveAction(QAState state, SuccessSaveAnswer action) {
  Navigator.of(action.context).pushNamed('/screen1');
  return state;
}
