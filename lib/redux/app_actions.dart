import 'package:flutter/material.dart';

class SaveAction {
  String actionQuestion;
  String actionAnswer;
  bool internetConnected;
  SaveAction(
    this.actionQuestion,
    this.actionAnswer,
    this.internetConnected,
  );
}

class SaveAsyncAction {
  String actionQuestion;
  String actionAnswer;
  SaveAsyncAction(
    this.actionQuestion,
    this.actionAnswer,
  );
}

class TryAgainAction {}

class SuccessSaveAnswer {
  BuildContext context;
  SuccessSaveAnswer(this.context);
}
