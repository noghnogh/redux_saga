class QAState {
  String question;
  String answer;
  bool isConnected;

  QAState(
      {required this.question,
      required this.answer,
      required this.isConnected});

  factory QAState.initial() {
    return QAState(question: '', answer: '', isConnected: true);
  }

  QAState copyWith({String? question, String? answer, bool? isConnected}) {
    return QAState(
        question: question ?? this.question,
        answer: answer ?? this.answer,
        isConnected: isConnected ?? this.isConnected);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is QAState &&
          runtimeType == other.runtimeType &&
          question == other.question &&
          answer == other.answer &&
          isConnected == other.isConnected;

  @override
  int get hashCode => super.hashCode;
}
